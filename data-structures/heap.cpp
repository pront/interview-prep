#include <vector>
#include <iostream>
#include <numeric>

using namespace std;

// Do not edit the class below except for the buildHeap,
// bubbleUp, bubbleDown, peek, extract, and insert methods.
// Feel free to add new properties and methods to the class.

template <typename T>
class Heap {
private:
    int leftChild(int idx) {
        int lc = 2 * idx + 1;
        return idx < heap.size() ? lc : INT_MAX;
    }

    int rightChild(int idx) {
        int rc = 2 * idx + 2;
        return rc < heap.size() ? rc : INT_MAX;
    }

    int getParent(int idx) {
        int p = (idx - 1) / 2;
        return p >= 0 ? p : INT_MAX;
    }

    // amortized O(N)
    void initHeap() {
        int firstIdx = getParent(heap.size() - 1);
        for (int curIdx = firstIdx; curIdx >= 0; curIdx--) {
            bubbleUp(curIdx);
        }
    }

    // O(logN)
    void bubbleUp(int currentIdx) {
        int endIdx = heap.size() - 1;
        int parentIdx = getParent(currentIdx);
        while (currentIdx != INT_MAX && heap[currentIdx] < heap[parentIdx]) {
            swap(heap[currentIdx], heap[parentIdx]);
            currentIdx = parentIdx;
            parentIdx = getParent(currentIdx);
        }
    }

    // O(logN)
    void bubbleDown(int currentIdx) {
        int endIdx = heap.size() - 1;
        int c1 = leftChild(currentIdx);

        while (c1 <= endIdx) {
            int c2 = rightChild(currentIdx);
            int idxOfMinVal;
            if (c2 != INT_MAX && heap[c2] < heap[c1]) {
                idxOfMinVal = c2;
            }
            else {
                idxOfMinVal = c1;
            }

            if (heap[idxOfMinVal] < heap[currentIdx]) {
                swap(heap[currentIdx], heap[idxOfMinVal]);
                currentIdx = idxOfMinVal;
                c1 = leftChild(currentIdx);
            }
            else {
                return;
            }
        }
    }

    vector<T> heap;

public:
    Heap() : heap() {}

    Heap(vector<T> v) : heap(v) {
        initHeap();
    }

    int peek() {
        return heap.size() > 0 ? heap[0] : -1;
    }

    int extract() {
        if (heap.empty()) return INT_MAX;

        swap(heap[0], heap.back());
        int v = heap.back();
        heap.pop_back();
        bubbleDown(0);
        return v;
    }

    void insert(int value) {
        heap.push_back(value);
        bubbleDown(heap.size() - 1);
    }

    bool empty() {
        return heap.empty();
    }
};


template <typename T>
void popAll(Heap<T>& h) {
    while (!h.empty()) {
        cout << "Extracted: " << h.extract() << endl;
    }
    cout << "Done.\n" << endl;
}

// TODO make it more generic
//template <typename T>
//struct MinComp {
//    bool operator()(const T& a, const T& b) {
//        return a < b;
//    }
//};

int main() {
    const int N = 10;
    vector<int> v(N);
    iota(v.begin(), v.end(), 0);

    Heap<int> minHeap(v);
    popAll(minHeap);

    return 0;
}
