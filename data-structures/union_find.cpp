class UnionFindSimple {
    vector<int> uf; // parents, union find

    void init(const vector<string>& words) {
        uf.resize(words.size());
        iota(uf.begin(), uf.end(), 0);
    }

    int find(int x) {
        if (x == uf[x]) return x;
        return uf[x] = find(uf[x]); // compress path and recurse
    }

    void unify(int x, int y) {
        int parent1 = find(x);
        int parent2 = find(y);
        if (parent1 != parent2) {
            uf[parent2] = parent1;
        }
    }
};

class UnionFind {
    vector<int> d_parents;
    vector<int> d_sizes;
    int d_numComponents;

public:
    UnionFind(int n) : d_parents(n),
                       d_sizes(n, 1),
                       d_numComponents(n) {
        iota(d_parents.begin(), d_parents.end(), 0);
    }

    int find(int x) {
        if (x == d_parents[x]) return x;
        return d_parents[x] = find(d_parents[x]); // compress path and recurse
    }

    bool unify(int x, int y) {
        int rootX = find(x);
        int rootY = find(y);
        if (rootX == rootY) return false;

        if (d_sizes[rootX] < d_sizes[rootY]) {
            d_parents[rootX] = d_parents[rootY];
            d_sizes[rootY] += d_sizes[rootX];
        }
        else {
            d_parents[rootY] = d_parents[rootX];
            d_sizes[rootX] += d_sizes[rootY];
        }

        d_numComponents--;
        return true;
    }

    int size(int x) {
        return d_sizes[find(x)];
    }
};
