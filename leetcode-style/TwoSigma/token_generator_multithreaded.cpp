#include <iostream>
#include <string>
#include <random>
#include <unordered_set>
#include <numeric>
#include <map>
#include <chrono>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>

using namespace std;

//TokenGenerator(int period, int capacity)
//Period: time needed to generate one token
//Capacity: max number of tokens the generator can hold

//Implement the function to get tokens from the generator:
//Int acquire(int n) : acquires n tokens, this should return:
//min(num tokens currently in the generator, n)

//Hint: So you remember the start time and the last time acquire() is called
//      to keep track of how many tokens the generator is currently holding

chrono::seconds Now() {
    return chrono::duration_cast<chrono::seconds>(chrono::system_clock::now().time_since_epoch());
}

int GenerateRandomNumber(int min, int max) {
    static random_device rd; // obtain a random number from hardware
    static default_random_engine eng(rd()); // seed the generator
    uniform_int_distribution<> dist(min, max); // generate a number between 0 and N
    return dist(eng);
}

class TokenGenerator {
    int period_;
    int capacity_;
    int tokens_remaining_;
    chrono::seconds last_called_;
    condition_variable not_empty_;
    condition_variable not_full_;
    mutex d_mutex;

public:
    TokenGenerator(int period, int capacity)
            : period_(period), capacity_(capacity), tokens_remaining_(capacity), last_called_(Now()), d_mutex() {
    };

    // returns the number of tokens currently in the generator
    pair<int, int> acquire(int n) {
        int taken = 0;
        while (taken != n) {
            lock_guard<mutex> lockGuard(d_mutex);
            if (tokens_remaining_ > n) {
                taken = n;
                tokens_remaining_ -= n;
            } else {
                taken = tokens_remaining_;
                tokens_remaining_ = 0;
            }
        }
        return {taken, tokens_remaining_};
    }

    void fill() {

    }
};

int main() {
    TokenGenerator tg(1, 100);
    vector<pair<int, int>> a = {{1,   0},
                                {3,   0},
                                {100, 3},
                                {1,   1}};
    for (const auto &p : a) {
        auto[taken, remaining] = tg.acquire(p.first);
        cout << "taken: " << taken << ", remaining: " << remaining << endl;
        this_thread::sleep_for(std::chrono::seconds(p.second));
    }
    return 0;
}