#include <iostream>
#include <string>
#include <random>
#include <unordered_set>
#include <numeric>
#include <map>
#include <chrono>
#include <thread>

using namespace std;

//TokenGenerator(int period, int capacity)
//Period: time needed to generate one token
//Capacity: max number of tokens the generator can hold

//Implement the function to get tokens from the generator:
//Int acquire(int n) : acquires n tokens, this should return:
//min(num tokens currently in the generator, n)

//Hint: So you remember the start time and the last time acquire() is called
//      to keep track of how many tokens the generator is currently holding

chrono::seconds Now() {
    return chrono::duration_cast<chrono::seconds>(chrono::system_clock::now().time_since_epoch());
}

class TokenGenerator {
    int period_;
    int capacity_;
    chrono::seconds lastCalled_;
    int tokens_remaining_;
public:
    TokenGenerator(int period, int capacity)
    : period_(period)
    , capacity_(capacity)
    , lastCalled_(Now())
    , tokens_remaining_(capacity) {
    };

    // returns the number of tokens currently in the generator
    int acquire(int n) {
        auto nowSecs = Now();
        auto diffSecs = (nowSecs - lastCalled_).count();
        if (diffSecs > period_) {
            int numPeriods = diffSecs / period_;
            cout << "numPeriods: " << numPeriods << endl;
            int sum = tokens_remaining_ + numPeriods;
            tokens_remaining_ = sum > capacity_ ? capacity_ : sum;
        }

        cout << "1) d_tokensRemaining:" << tokens_remaining_ << endl;
        if (tokens_remaining_ > n) {
            tokens_remaining_ -= n;
        }
        else {
            tokens_remaining_ = 0;
        }

        cout << "2) d_tokensRemaining:" << tokens_remaining_ << endl;
        lastCalled_ = nowSecs;
        return tokens_remaining_;
    }
};

int main() {
    TokenGenerator tg(1, 100);
    cout << "Tokens remaining: \n" << tg.acquire(101) << endl;
    this_thread::sleep_for(std::chrono::seconds (3));
    cout << "Tokens remaining: \n" << tg.acquire(2) << endl;
    return 0;
}