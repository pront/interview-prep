#include <map>
#include <set>
#include <list>
#include <string>
#include <bitset>
#include <limits>
#include <vector>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>

using namespace std;

class PhoneDirectoryFast {
    int d_max;
    unordered_set<int> d_used;
    list<int> d_released;

public:
    /** Initialize your data structure here
        @param maxNumbers - The maximum numbers that can be stored in the phone directory. */
    PhoneDirectoryFast(int maxNumbers) : d_max(maxNumbers), d_used(), d_released() {
    }

    /** Provide a number which is not assigned to anyone.
        @return - Return an available number. Return -1 if none is available. */
    int get() {
        if (d_used.size() == d_max) return -1;

        int val;
        if (d_released.empty()) {
            val = d_used.size();
        } else {
            val = d_released.front();
            d_released.pop_front();
        }
        d_used.insert(val);
        return val;
    }

    /** Check if a number is available or not. */
    bool check(int number) {
        if (number < 0 || number >= d_max) return false;
        return d_used.count(number) == 0;
    }

    /** Recycle or release a number. */
    void release(int number) {
        if (d_used.count(number) == 0) return;
        d_released.push_back(number);
        d_used.erase(number);
    }
};

class PhoneDirectorySpaceEfficient {
private:
    bitset<1000> bits;
    int maxT;
    int cur;
public:
    /** Initialize your data structure here
        @param maxNumbers - The maximum numbers that can be stored in the phone directory. */
    PhoneDirectorySpaceEfficient(int maxNumbers) {
        maxT = maxNumbers;
        cur = 0;
    }

    /** Provide a number which is not assigned to anyone.
        @return - Return an available number. Return -1 if none is available. */
    int get() {
        if (cur == maxT)
            return -1;
        int res = cur;
        bits[res] = true;
        for (; cur < maxT; cur++) {
            if (!bits[cur])
                break;
        }
        return res;
    }

    /** Check if a number is available or not. */
    bool check(int number) {
        if (number < 0 || number >= maxT)
            return false;
        return !bits[number];
    }

    /** Recycle or release a number. */
    void release(int number) {
        if (number < 0 || number >= maxT || !bits[number])
            return;
        bits[number] = false;
        cur = min(cur, number);
    }
};

class AllocatorHeap {
private:
    int MAX_ID = 1000;
    bitset<1000> bitSet;

public:
    int allocate() {
        int index = 0;
        while (index < MAX_ID - 1) {
            if (!bitSet[index * 2 + 1]) {
                index = index * 2 + 1;
            } else if (!bitSet[index * 2 + 2]) {
                index = index * 2 + 2;
            } else {
                return -1;
            }
        }
        bitSet[index] = true;
        updateTree(index);
        return index - MAX_ID + 1;
    }

    void updateTree(int index) {
        while (index > 0) {
            int parent = (index - 1) / 2;
            if (index % 2 == 1) { //left child
                if (bitSet[index] && bitSet[index + 1]) {
                    bitSet[parent] = true;
                } else {
                    bitSet[parent] = false; //it is required for release id
                }
            } else {
                if (bitSet[index] && bitSet[index - 1]) {
                    bitSet[parent] = true;
                } else {
                    bitSet[parent] = false;
                }
            }
            index = parent;
        }
    }

    void release(int id) {
        if (id < 0 || id >= MAX_ID) return;
        if (bitSet[id + MAX_ID - 1]) {
            bitSet[id + MAX_ID - 1] = false;
            updateTree(id + MAX_ID - 1);
        }
    }

    bool check(int id) {
        if (id < 0 || id >= MAX_ID) return false;
        return !bitSet[id + MAX_ID - 1];
    }
};

int main() {
    return 0;
}