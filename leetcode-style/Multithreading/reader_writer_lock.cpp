#include <iostream>
#include <string>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <thread>
#include <variant>
#include <limits>
#include <random>
#include <exception>
#include <chrono>

class NaiveReaderWriterLock {
public:
    void AcquireReadLock() {
        std::lock_guard<std::mutex> lg(num_readers_mutex_);
        num_readers_++;
        if (num_readers_ == 1) {
            write_mutex_.lock();
        }
    }

    void ReleaseReadLock() {
        std::lock_guard<std::mutex> lg(num_readers_mutex_);
        num_readers_--;
        if (num_readers_ == 0) {
            write_mutex_.unlock();
        }
    }

    void AcquireWriteLock() {
        write_mutex_.lock();
    }

    void ReleaseWriteLock() {
        write_mutex_.unlock();
    }

private:
    int num_readers_ = 0;
    std::mutex num_readers_mutex_;
    std::mutex write_mutex_;
};

class ReadWriteLockWithWriterPriority {
public:
    void AcquireReadLock() {
        std::unique_lock<std::mutex> lock(mutex_);
        waiting_readers_cv_.wait(lock, [this] { return !writer_active_ && writers_waiting_ == 0; });
        ++num_readers_;
    }

    void ReleaseReadLock() {
        std::unique_lock<std::mutex> lock(mutex_);
        --num_readers_;
        if (num_readers_ == 0 && writers_waiting_ > 0) {
            lock.unlock();
            waiting_writers_.notify_one();
        }
    }

    void AcquireWriteLock() {
        std::unique_lock<std::mutex> lock(mutex_);
        ++writers_waiting_;
        waiting_writers_.wait(lock, [this] { return !writer_active_ && num_readers_ == 0; });
        --writers_waiting_;
        writer_active_ = true;
    }

    void ReleaseWriteLock() {
        std::unique_lock<std::mutex> lock(mutex_);

        if (writers_waiting_ > 0) {
            lock.unlock();
            waiting_writers_.notify_one();
        } else {
            writer_active_ = false;
            lock.unlock();
            waiting_readers_cv_.notify_all();
        }
    }

private:
    std::mutex mutex_;
    std::condition_variable waiting_readers_cv_;
    std::condition_variable waiting_writers_;

    int num_readers_ = 0;
    int writers_waiting_ = 0;
    bool writer_active_ = false;
};

int main() {
    ReadWriteLockWithWriterPriority rw_lock;
    std::thread t1([&]() {
        rw_lock.AcquireReadLock();
        rw_lock.AcquireWriteLock();
        std::this_thread::sleep_for(std::chrono::seconds(2));
        rw_lock.ReleaseWriteLock();
        rw_lock.ReleaseReadLock();
    });

    std::thread t2([&]() {
        rw_lock.ReleaseReadLock();
    });

    t1.join();
    t2.join();
    std::cout << "DONE." << std::endl;
    return 0;
}