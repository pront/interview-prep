#include <iostream>
#include <string>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <thread>
#include <variant>
#include <limits>
#include <random>
#include <exception>
#include <chrono>

int GetRandomNumber(int min, int max) {
    static std::random_device rd; // obtain a random number from hardware
    static std::default_random_engine eng(rd()); // seed the generator
    std::uniform_int_distribution<> dist(min, max); // generate a number between 0 and N
    return dist(eng);
}

using namespace std::chrono;

milliseconds GetCurrentTimeInMs() {
    // get current time
    auto now = system_clock::now();

    // get number of milliseconds for the current second
    // (remainder after division into seconds)
    auto ms = duration_cast<milliseconds>(now.time_since_epoch());
    std::cout << ms.count() << std::endl;
    return ms;
}

class TokenBucket {
public:
    TokenBucket(int capacity, int rate) : capacity_(capacity), fill_rate_per_second_(rate),
                                          last_fill_ms_(GetCurrentTimeInMs()) {}

    void Fill() {
        std::unique_lock<std::mutex> ul(mutex_);
        not_full_.wait(ul, [this]() { return bucket_.size() < capacity_; });

        auto ms_now = GetCurrentTimeInMs();
        size_t refill_count = ((ms_now - last_fill_ms_).count() / 1000) *fill_rate_per_second_;
        size_t tokens_to_add = std::min(capacity_ - bucket_.size(), refill_count);
        last_fill_ms_ = ms_now;
        while (tokens_to_add-- >= 0) {
            bucket_.push(GetRandomNumber(0, 10));
        }
        std::cout << "Bucket now has " << bucket_.size() << " tokens" << std::endl;
        ul.unlock();
        not_empty_.notify_all();
    }

    std::vector<int> Retrieve(int n) {
        std::vector<int> tokens;
        while (n-- > 0) {
            std::unique_lock<std::mutex> ul(mutex_);
            not_empty_.wait(ul, [this]() { return !bucket_.empty(); });
            tokens.push_back(bucket_.front());
            bucket_.pop();
            ul.unlock();
            not_full_.notify_one();
        }
        return tokens;
    }

private:
    size_t capacity_;
    std::queue<int> bucket_;
    size_t fill_rate_per_second_;
    std::chrono::milliseconds last_fill_ms_;
    std::mutex mutex_;
    std::condition_variable not_full_;
    std::condition_variable not_empty_;
};

int main() {
    TokenBucket token_bucket(100, 10);
    std::thread filler([&]() {
        int count = 10;
        while (count-- > 0) {
            std::this_thread::sleep_for(500ms);
            token_bucket.Fill();
        }
    });

    std::thread consumer([&]() {
        int count = 10;
        while (count-- > 0) {
            auto tokens = token_bucket.Retrieve(5);
            for (int t : tokens) {
                std::cout << "Got token: " << t << std::endl;
            }
            std::this_thread::sleep_for(100ms);
        }
    });

    filler.join();
    consumer.join();
    return 0;
}