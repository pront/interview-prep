#include <iostream>
#include <string>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <thread>
#include <variant>
#include <limits>
#include <random>

using namespace std;

int GetRandomInt(int min = 0, int max = 500) {
    static std::random_device dev;
    static std::mt19937 rng(dev());
    std::uniform_int_distribution<int> d(min, max); // distribution in range [1, 6]
    return d(rng);
}

template<typename T>
class BlockingQueue {
public:
    enum class Result {
        TIMED_OUT,
        SUCCESS
    };

    explicit BlockingQueue(size_t maxSize) : max_size_(maxSize), mutex_(), cv_(), queue_() {}

    Result Push(const T &e, std::chrono::milliseconds timeout_duration = 1000ms) {
        std::unique_lock<std::mutex> lk(mutex_);
        const bool expired = !cv_.wait_for(lk, timeout_duration, [this] { return queue_.size() != max_size_; });
        if (expired) {
            std::cerr << std::this_thread::get_id() << " timed out" << std::endl;
            return Result::TIMED_OUT;
        }
        queue_.push(e);

        // see notify_one() docs
        lk.unlock();
        cv_.notify_all();
        return Result::SUCCESS;
    }

    Result Pop(T *val, std::chrono::milliseconds timeout_duration = 1000ms) {
        std::unique_lock<std::mutex> lk(mutex_);
        const bool expired = !cv_.wait_for(lk, timeout_duration, [this] { return !queue_.empty(); });
        if (expired) {
            std::cerr << std::this_thread::get_id() << " timed out" << std::endl;
            return Result::TIMED_OUT;
        }
        // get local copy and extract from queue
        *val = queue_.front();
        queue_.pop();

        lk.unlock();
        cv_.notify_all();
        return Result::SUCCESS;
    }

    size_t size() {
        std::unique_lock<std::mutex> lk(mutex_);
        return queue_.size();
    }

private:
    size_t max_size_;
    mutex mutex_;
    condition_variable cv_;
    queue<T> queue_;
};

int main() {
    int maxSz = 4;
    BlockingQueue<int> q(maxSz);

    auto push_with_retries = [](int i, BlockingQueue<int> &q) {
        int retries = 3;
        while (retries-- > 0) {
            if (q.Push(i) == BlockingQueue<int>::Result::SUCCESS) {
                return true;
            }
            const int sleep_ms = GetRandomInt();
            std::this_thread::sleep_for((sleep_ms) * 1ms);
        }
        return false;
    };

    thread producer1([&]() {
        for (int i = 0; i < 100; ++i) {
            if (i % 2 == 0) {
                if (!push_with_retries(i, q)) break;
            }
        }
        std::cout << "Producer1 done." << std::endl;
    });

    thread producer2([&]() {
        for (int i = 0; i < 100; ++i) {
            if (i % 2 == 1) {
                if (!push_with_retries(i, q)) break;
            }
        }
        std::cout << "Producer2 done." << std::endl;
    });

    thread consumer1([&]() {
        while (q.size() > 0) {
            int v = std::numeric_limits<int>::min();
            auto result = q.Pop(&v);
            if (result == BlockingQueue<int>::Result::SUCCESS) {
                std::cout << "Consumed " << v << std::endl;
            }
        }
        std::cout << "Consumer1 done." << std::endl;
    });

    producer1.join();
    producer2.join();
    consumer1.join();
    std::cout << q.size() << " elements left in the queue." << std::endl;
    return 0;
}